const request = require("supertest");
const app = require('../src/app')
const User = require("../src/models/user")
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");

const userOneId = new mongoose.Types.ObjectId();


const userOne = {
    _id: userOneId,
    name: "Mike",
    email: "mike@exe.com",
    password: "abcd123",
    tokens: [{
        token: jwt.sign({ _id: userOneId }, "thisismynewcourse")
    }]
}

beforeEach(async () => {
    await User.deleteMany()
    await new User(userOne).save()
})


test("should signup", async () => {
    await request(app).post("/users").send({
        name: "mike",
        email: "mike@gmail.com",
        password: "mike1234"
    }).expect(201)
})

test("should Login", async () => {
    const response = await request(app).post("/users/login").send({
        email: userOne.email,
        password: userOne.password
    }).expect(200)

    // Assert that the database was changed correctly
    const user = await User.findById(response.body.user._id)
    expect(user).not.toBeNull()
})

test("Should not login", async () => {
    await request(app).post("/users/login").send({
        email: userOne.email,
        password: "thisismycourse"
    }).expect(400)
})

test("Should get profile for user", async () => {
    await request(app)
        .get("/users/me")
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)

})

// test("should show avatar", async () => {
//     await request(app)
//         .post("/users/me/avatar")
//         .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
//         .attach('avatar', 'test/fixtures/profile-pic.jpg')
//         .expect(200)
// })