const Tasks = require('../models/tasks')

const createTask = (async (req, res) => {
    const task = new Tasks({
        ...req.body,
        owner: req.user._id
    })
    try {
        await task.save();
        res.status(201).send(task)
    } catch (error) {
        res.status(500).send(error);
    }
})

const readTask = (async (req, res) => {
    const match = {}
    if (req.query.completed) {
        match.completed = req.query.completed === 'true'
    }
    try {
        const tasks = await Tasks.find({ owner: req.user._id })
        res.status(200).send(tasks)
    } catch (error) {
        res.status(500).send(error)
    }
})

const readSingleTask = (async (req, res) => {
    const _id = req.params.id;
    try {
        const task = await Tasks.findOne({ _id, owner: req.user._id })
        if (!task) {
            res.status(404).send("Task not found!");
        }
        res.send(task);
    } catch (error) {
        console.log(error);
    }
})

const updateTask = (async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdate = ['desc', 'completed'];
    const validOperation = updates.every((update) => allowedUpdate.includes(update));
    if (!validOperation) {
        res.status(400).send({ error: "Invalid Updates" })
    }
    try {
        const task = await Tasks.findOne({ _id: req.params.id, owner: req.user._id })
        if (!task) {
            res.status(404).send();
        }
        updates.forEach((update) => task[update] = req.body[update])
        await task.save()
        res.send(task);
    } catch (error) {
        res.status(400).send()
    }
})

const deleteTask = (async (req, res) => {
    try {
        const task = await Tasks.findOneAndDelete({ _id: req.params.id, owner: req.user._id })
        if (!task) {
            res.status(404).send("Task not found");
        }
        res.send(task);
    } catch (error) {
        res.status(400).send()
    }
})
module.exports = { createTask, readTask, readSingleTask, updateTask, deleteTask }